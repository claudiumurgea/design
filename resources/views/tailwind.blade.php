<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>Laravel</title>
        <script src="https://cdn.tailwindcss.com"></script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="css/app.css">
        <link rel="stylesheet" href="css/custom.css">
    </head>
    <body>
      {{--  style=" transform: scale(0.8);" --}}
    <div class="custom__color5 mx-1 pt-8 mt-2 px-4
      md:mx-6 md:mt-6 md:px-12
      lg:mx-8 lg:mt-8 lg:px-16">

      {{-- <div class="w-36 py-8 sm:py-28 md:py-40 xl:py-56 2xl:py-64 bg-no-repeat bg-cover" style="background-image:url(../images/MediumLogo.PNG);background-size:100% 100%"></div> --}}

      <div class="flex">
          <div class="-rotate-90 w-0 xs2 translate-x-2.5 translate-y-2
            md:text-sm md:translate-x-4 md:translate-y-2.5
            lg:text-lg lg:translate-x-7 lg:translate-y-3.5
            ">
              THE
          </div>
          <h1 class="montecatini text-4xl
            md:text-6xl 
            lg:text-8xl
            ">
              DOYENNE
          </h1>
      </div>

      <div class="custom__color affable text-xl translate-x-2 -translate-y-4
        md:text-4xl md:translate-x-2 md:-translate-y-5
        lg:text-6xl lg:translate-x-4 lg:-translate-y-8
        ">
          Agency
      </div>

      <div class="xs3 translate-x-11 -translate-y-10 inline-block
        md:scale-125 md:translate-x-20 md:-translate-y-10
        lg:scale-75 lg:text-xs lg:translate-x-32 lg:-translate-y-12
        ">
          GLOBAL SALES
      </div>

      <div class="xs3 translate-x-14 -translate-y-10 inline-block
        md:scale-125 md:translate-x-32 md:-translate-y-10
        lg:scale-75 lg:text-xs lg:translate-x-36 lg:-translate-y-12
        ">
          AND SALES TRAINING
      </div>

      <div class="mt-4
        md:mx-12 md:mt-12
        lg:mx-20 lg:mt-20
        2xl:ml-36
        ">
          <h2 class="arno border-b border-pink-500 text-2xl
            sm:text-3xl
            md:text-4xl
            lg:text-5xl
            xl:text-6xl
            2xl:text-8xl
            ">
              SURGE
          </h2>

          <p class="custom__color text-base sm:tracking-normal mt-3
            md:text-lg md:tracking-widest md:mt-5
            lg:text-xl lg:tracking-widesst lg:mt-7
            ">
              5 KEYS TO END THE MADDENING CYCLE OF FEAST OR FAMINE IN YOUR BUSINESS
          </p>
          <button class="w-2/5 arno custom__color2 text-white font-bold rounded-md text-lg px-4 mt-6 pt-2 pb-0.5
            sm:text-xl sm:mt-8 sm:pt-3 sm:pb-1.5
            md:text-2xl md:mt-10 md:pt-4 md:pb-2
            lg:text-3xl lg:mt-10
            xl:text-4xl xl:mt-16 xl:pt-6 xl:pb-3.5
            2xl:text-5xl 2xl:my-12 2xl:pt-8 2xl:pb-5
            ">
              REGISTER &nbsp;NOW
          </button>
      </div>

      <div class="flex translate-y-2/4 space-x-4 mt-12 mt-1
        md:mx-12 md:-mt-8 md:gap-x-4
        lg:mx-20 lg:-mt-4
        xl:gap-x-8
        2xl:gap-x-12 2xl:mx-36
        ">
          <div class="custom__color2 rounded-lg w-2/6">
            <div class="text-white text-center font-bold text-base">
              <p class="text-sm pt-2 px-4
                sm:text-base sm:pt-6 sm:px-12
                md:text-lg md:pt-8 md:px-6
                lg:text-xl lg:pt-12 lg:px-10
                xl:text-2xl xl:pt-12 xl:px-16 
                2xl:text-4xl 2xl:pt-24 2xl:pb-2 2xl:px-20 
                ">
                  Did you work hard
              </p>
              <p class="font-light text-xs pt-2 pb-6
                sm:px-6
                md:text-sm md:font-normal md:pt-3 md:pb-8 md:px-3
                lg:text-base lg:font-normal lg:pb-4 lg:pb-12 lg:px-4
                xl:text-lg xl:pb-12 xl:px-16
                2xl:text-xl 2xl:pb-28 2xl:px-20 
                ">
                  &nbsp; to atract leads only to learn they weren't qualified or ideal?</p>
            </div>
          </div>
          <div class="custom__color2 rounded-lg w-2/6">
            <div class="text-white text-center font-bold text-base">
              <p class="text-sm pt-2 px-4
              sm:text-base sm:pt-6 sm:px-12
              md:text-lg md:pt-8 md:px-6
              lg:text-xl lg:pt-12 lg:px-10
              xl:text-2xl xl:pt-12 xl:px-16 
              2xl:text-4xl 2xl:pt-24 2xl:pb-2 2xl:px-24
                ">
                  Are you exhausted
              </p>
              <p class="font-light text-xs pt-2 pb-6
                sm:px-6
                md:text-sm md:font-normal md:pt-3 md:pb-8 md:px-3
                lg:text-base lg:font-normal lg:pb-4 lg:pb-12 lg:px-4
                xl:text-lg xl:pb-12 xl:px-12
                2xl:text-xl 2xl:pb-28 2xl:px-20 
                ">
                  from the up and down in your revenue and you're ready for it to end?
              </p>
            </div>
           
          </div>
          <div class="custom__color2 rounded-lg w-2/6">
            <div class="text-white text-center font-bold text-base">
              <p class="text-sm pt-2 px-3
              sm:text-base sm:pt-6 sm:px-12
              md:text-lg md:pt-8 md:px-6
              lg:text-xl lg:pt-12 lg:px-10
              xl:text-2xl xl:pt-12 xl:px-14 
              2xl:text-4xl 2xl:pt-24 2xl:pb-2 2xl:px-20
                ">
                  Did you believe that
              </p>
              <p class="font-light text-xs pt-2 pb-6
                sm:px-6
                md:text-sm md:font-normal md:pt-3 md:pb-8 md:px-3
                lg:text-base lg:font-normal lg:pb-4 lg:pb-12 lg:px-4
                xl:text-lg xl:pb-12 xl:px-16
                2xl:text-xl 2xl:pb-28 2xl:px-20 
                ">
                  once you finally got six-figures, you would be able to disembark from the revenue rollercoaster only to realise that's NOT true?
              </p>
            </div>
           
          </div>
      </div>

    </div>
      
      <section class="flex mt-48 ml-6 space-x-6
        md:ml-28 md:mt-60 md:space-x-9
        lg:ml-48 lg:mt-60 lg:space-x-12
        xl:mt-72
        2xl:mt-96
        ">
        <div class="w-3/6 md:ml-2 lg:-ml-4">
          <div class="tracking-widest text-base pt-8
            md:text-lg md:pt-10
            lg:text-xl lg:pt-12
            xl:text-2xl xl:pt-16
            2xl:text-3xl 2xl:pt-24">
              INTRODUCING
          </div>
          <div class="arno custom__color text-2xl  
            md:text-3xl
            lg:text-4xl
            xl:text-4xl
            2xl:text-5xl">
              SURGE...
          </div>
          <div class="text-xs pt-2 pb-6
            md:text-sm md:pb-9 
            lg:text-base lg:pb-12
            xl:text-lg xl:pb-16
            2xl:text-xl 2xl:pb-24">
              a one-day virtual event that will help you put an end to the cycle of feast and famine in your business 
              so that you can create a steady consistent stream of ideal clients and revenue in your business.
          </div>
        </div>
        <div class="w-10/12 bg-gray-200 border-2 border-solid border-gray-400 border-r-0">
        </div>
      </section>

      <section class="flex mt-20
        sm:mt-24 
        md:mt-28
        lg:mt-32
        xl:mt-36
        2xl:mt-40
        ">
      {{-- translate-x-6 scale-x-110 --}}
          <div class="custom__color2 pl-8 scale-x-110
            sm:pl-10
            md:pl-20
            lg:pl-24 lg:scale-x-125
            xl:pl-28 xl:scale-x-125
            2xl:pl-32 2xl:scale-x-125
            ">
            <div class="text-white tracking-widest text-base pt-2 scale-x-100
              sm:text-base sm:pt-8
              md:text-lg md:pt-10 md:pl-10
              lg:text-xl lg:pt-20 lg:pl-24
              xl:text-2xl xl:pt-28 xl:pl-30
              2xl:text-3xl 2xl:pt-36 2xl:pl-30
              ">
                YOUR
            </div>
            <div class="arno text-2xl scale-x-100
              sm:text-3xl
              md:text-4xl md:pt-1 md:pl-10
              lg:text-5xl lg:pt-1 lg:pl-24
              xl:text-6xl xl:pt-1 xl:pl-30
              2xl:text-7xl 2xl:pt-2 2xl:pl-30
              ">
                SURGE Guide...
            </div>
            <div class="text-white pt-1 pb-3 text-xs pr-2 scale-x-100 pr-6
              sm:pr-12 sm:pt-4 sm:pb-8
              md:text-sm md:pl-10 md:pb-16 md:pr-8
              lg:text-base lg:pl-24  lg:pr-28
              xl:text-lg xl:pl-30 xl:pr-36
              2xl:text-xl 2xl:pl-30 2xl:pr-56
              ">
                My name is Dr. Nadia Brown and I am the CEO of the Doyenne Agency and we specialize in helping clients have 
                  the courageous sales conversations needed to create consistent cash flow in their business. 
                We help our service-based clients increase their sales results by teaching them and their teams how to 
                  lead sales conversations, adjust their pricing and create deep relationships with prospects and clients. 
                Our clients have credited us directly for rasing their rates, decreasing their refund requests, 
                  and doubling or even tripling their anual revenue.
            </div>
          </div>
          <div class="flex w-full bg-cover bg-no-repeat translate-y-6 px-28
            sm:px-36
            md:translate-y-10 md:px-48
            lg:translate-y-16 lg:py-72
            xl:py-80 xl:translate-y-24
            2xl:py-96 2xl:translate-y-28
            " style="background-image: url(../images/Doyenne.PNG); background-size:100% 100%"> </div>
      </section>

      <p class="arno w-full text-center text-lg pt-24 mb-2 px-4
        sm:px-5
        md:text-xl md:pt-28 md:px-20
        lg:text-2xl lg:px-28
        xl:text-2xl xl:px-36 xl:pt-36
        2xl:text-3xl 2xl:px-44 2xl:pt-36
        ">
          In this one-day training, I am going to share with you how you can better qualify your leads, structure your sales 
          conversations and create a strategy to create consistent revenue in your business.
      </p>

      <div class="flex m-4 justify-between
        sm:m-6
        md:mx-28
        lg:mx-48
        xl:pt-10
        2xl:pt-12
        ">
        <div class="w-1/6 bg-no-repeat py-16 sm:py-20 md:py-24 lg:py-28 xl:py-32 2xl:py-36" style="background-image: url(../svg/svg1.SVG); background-size:100% 100%"></div>
        <div class="w-1/6 bg-no-repeat py-16 sm:py-20 md:py-24 lg:py-28 xl:py-32 2xl:py-36" style="background-image: url(../svg/svg2.SVG); background-size:100% 100%"></div>
        <div class="w-1/6 bg-no-repeat py-16 sm:py-20 md:py-24 lg:py-28 xl:py-32 2xl:py-36" style="background-image: url(../svg/svg3.SVG); background-size:100% 100%"></div>
      </div>
  
      <section class="w-full custom__color5">
        <p class="arno text-center text-lg pt-6 px-20
          sm:pt-10
          md:text-xl md:pt-12
          lg:text-2xl lg:pt-16
          xl:text-3xl xl:mt-10 xl:pt-20
          2xl:text-3xl 2xl:mt-16 2xl:pt-24
          ">
            That's right, you can finally end the madness of feast or famine revenue in your business.
        </p>
        <p class="text-base text-center pt-2 px-4 pb-6
          sm:pt-4 sm:pb-10
          md:px-20 md:pt-6 md:pb-12
          lg:text-lg lg:px-28 lg:pb-16
          xl:px-36
          2xl:px-40
          ">
            AND, you have an opportunity to <span class="custom__color font-bold">apply to join me for my BONUS VIP Private Masterclass</span> on Wednesday, March 
            24th, 2021
            at no additional investment (more details provided after you ssecure your seat.)
        </p>
      </section>

      <div class="flex justify-center pt-3 mx-5
        sm:pt-4 
        md:pt-1 md:mx-28 md:px-2
        lg:pt-5 lg:pb-14 lg:mx-40
        xl:pt-10 
        2xl:pt-20
        ">
        <button class="w-2/5 arno custom__color2 text-white font-bold rounded-md text-lg px-4 mt-6 pt-2 pb-0.5
            sm:text-xl sm:mt-8 sm:pt-3 sm:pb-1.5
            md:text-2xl md:mt-10 md:pt-4 md:pb-2
            lg:text-3xl lg:mt-10
            xl:text-4xl xl:mt-16 xl:pt-6 xl:pb-3.5
            2xl:text-5xl 2xl:my-12 2xl:pt-8 2xl:pb-5
            ">
            REGISTER&nbsp; NOW
        </button>
      </div>

      <div class="flex justify-end mt-12">
          <img class="w-2/6 z-10 order-1 translate-x-20
            sm:translate-x-24
            md:translate-x-28
            lg:translate-x-48 lg:scale-90 lg:-translate-y-16
            xl:translate-x-3/4 xl:scale-75 xl:-translate-y-16" src="{{ url("images/client.PNG") }}" alt="img">

          <div class="w-4/6 z-0 order-2 bg-slate-600 translate-y-12
            lg:scale-y-90
            xl:scale-y-75
            ">
            <div class="ml-24 pt-4
              sm:ml-32 sm:pt-4
              md:ml-40 md:pt-8
              lg:ml-64 lg:pt-16
              xl:ml-80 xl:pl-14
              2xl:ml-96 2xl:pl-24 2xl:pt-24
              ">
              <h1 class="text-lg custom__color
                sm:text-xl sm:pt-10
                md:text-2xl
                lg:text-3xl
                xl:text-4xl xl:pt-20
                2xl:text-5xl
                ">
                  La'Vista Jones
              </h1>
              <h2 class="text-white text-base
                sm:text-lg
                md:text-xl
                lg:text-2xl
                xl:text-2xl
                2xl:text-3xl 2xl:tracking-widest
                ">
                  OPERATIONS MANAGER
              </h2>
              <h2 class="text-white text-base
                sm:text-lg
                md:text-xl
                lg:text-2xl
                xl:text-2xl
                2xl:text-3xl 2xl:tracking-widest
                ">
                  31 MARKETPLACE
              </h2>
              <p class="text-white text-sm py-2 pr-2
                sm:text-base sm:pt-2 sm:pr-10
                md:text-lg md:pr-20
                lg:text-xl lg:pt-8 lg:pr-30
                xl:text-2xl xl:pt-12 xl:pr-40
                2xl:text-3xl 2xl:pr-24
                ">
                  By simply following the steps Dr. Nadia provided for having sales conversations...
                <span class="font-bold">I was able to increase my monthly revenue by &nbsp;
                  <span class="absolute translate-y-0.5 -translate-x-2
                    sm:-translate-x-2.5
                    lg:-translate-x-3
                    xl:-translate-x-3.5
                    2xl:translate-y-1 2xl:-translate-x-4">
                    4
                  </span>80%!</span> 
              </p>
            </div>
          </div>

      </div>

      <section class="flex w-full z-10 pt-16 
        sm:pt-20
        md:pt-24
        lg:pt-28
        ">
        <div class="bg-cover w-4/6 bg-no-repeat z-20 order-2 scale-y-50  -translate-x-3 -translate-y-32 z-30
          sm:scale-x-110 sm:scale-y-75 sm:-translate-y-1 sm:-translate-x-1
          md:scale-x-100 md:scale-y-90 md:-translate-x-5 md:translate-y-20
          lg:scale-x-90 lg:-translate-x-12 lg:translate-y-28
          xl:scale-x-75 xl:-translate-x-36 xl:translate-y-32
          2xl:scale-x-50 2xl:scale-y-100 2xl:-translate-x-80 2xl:translate-y-36" style="background-image: url(../images/surgeWoman.PNG);background-size:125% 75%">
        </div>

          <div class="flex">
            <div class="custom__color2 w-full z-20 order-1">
              <div class="arno text-white text-3xl w-5/6 xl:w-3/4 border-b-2 border-white pt-6 ml-6 
                sm:text-3xl sm:pt-8 sm:ml-10
                md:textg-4xl md:pt-10 md:ml-11
                lg:text-5xl lg:pt-14 lg:ml-16
                xl:text-6xl xl:pt-16 xl:ml-12
                2xl:text-8xl 2xl:ml-16
                ">
                  SURGE
              </div>
              <div class="text-white text-base ml-6 tracking-wide
                sm:text-lg sm:ml-10
                md:ml-11
                lg:text-xl lg:ml-16 lg:pb-4
                xl:pb-8 xl:ml-12
                2xl:text-2xl 2xl:ml-16
                ">
                  IS FOR YOU IF:
              </div>
            <div class="flex flex-col ml-2 mt-6 mb-8 gap-4 text-white text-xs mr-8
              sm:mr-8 sm:text-sm sm:py-2
              md:mr-10 md:text-base md:py-3
              lg:mr-12 lg:text-lg lg:py-4
              xl:mr-40 xl:py-6 xl:mb-12
              2xl:mr-44 2xl:pb-8 2xl:gap-8
              ">
              <div class="flex space-x-10">
                <div id="pointer" class="w-10 custom__color4 opacity-40"></div>
                <div class="w-full custom__color3 pl-2 pr-3 xl:pr-0 h-20 justify-center">
                  You're tired of the inconsistency in your business even though you're working really hard doing ALL of  the things.
                </div>
              </div>

              <div class="flex space-x-10">
                <div id="pointer" class="w-10 custom__color4 opacity-40"></div>
                <div class="w-full custom__color3 pl-3 pr-4 py-1 h-20">
                  You are ready to build the business you've always dreamed of.
                </div>
              </div>
              

              <div class="flex flex-row space-x-10">
                <div id="pointer" class="w-10 custom__color4 opacity-40"></div>
                <div class="w-full custom__color3 pl-3 pr-4 py-1 h-20">
                  You're ready to generate the income you desire and set new records in your business.
                </div>
              </div>

              <div class="flex flex-row space-x-10">
                <div id="pointer" class="w-10 custom__color4 opacity-40"></div>
                <div class="w-full custom__color3 pl-3 pr-4 py-1 h-20">
                  You know it doesn't have to be this way.
                </div>
              </div>

              <div class="flex flex-row space-x-10">
                <div id="pointer" class="w-10 custom__color4 opacity-40"></div>
                <div class="w-full custom__color3 pl-3 pr-4 py-1 h-20">
                  You're tired of working with less than ideal clients or let's face it.
                </div>
              </div>

              <div class="flex flex-row space-x-10">
                <div id="pointer" class="w-10 custom__color4 opacity-40"></div>
                <div class="w-full custom__color3 pl-3 pr-4  h-20">
                  You're tired of working with clients you don't like and who don't value or respect your time and contribution.
                </div>
              </div>

              <div class="flex flex-row space-x-10">
                <div id="pointer" class="w-10 custom__color4 opacity-40 "></div>
                <div class="w-full custom__color3 pl-3 pr-4 py-1 h-20">
                  You sell (or desire to sell) a program or service that is $5,000 or more.
                </div>
              </div>

              <div class="flex flex-row space-x-10">
                <div id="pointer" class="w-10 custom__color4 opacity-40"></div>
                <div class="w-full custom__color3 pl-3 pr-4 py-1 h-20">
                  You're tired of playing small.
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div class="lg:custom__color5 lg:py-2 m-0 lg:-translate-y-full
        2xl:custom__color5 2xl:py-56 2xl:lg:py-2 2xl:-translate-y-full">
      </div>

      <div class="custom__color5 lg:-translate-y-2/4
        lg:flex lg:flex-col lg:float-right lg:w-4/6">
        <div class="arno tracking-wide text-lg ml-16 pb-6 pt-36 -translate-y-14
          sm:text-xl sm:-translate-y-16
          md:text-2xl md:-translate-y-14
          lg:text-3xl lg:translate-y-8 lg:translate-x0 lg:pt-96 lg:mt-64
          xl:pt-72 xl:translate-y-10 xl:mt-80
          2xl:pt-80 2xl:mt-72 2xl:pb-10
          ">
            Here's what you'll learn:
        </div>

        <div class="flex flex-col ml-2 mb-4 gap-3 text-xs -translate-y-12
          sm:text-sm sm:-translate-y-12 
          md:text-base md:-translate-y-14
          lg:text-lg lg:translate-y-0 lg:translate-x-0 lg:pt-6
          xl:pt-8
          xl:text-xl
          2xl:text-xl">

          <div class="flex flex-row space-x-5 pl-12
          ">
            <div class="p-9 bg-no-repeat" style="background-image:url(../images/checked.PNG)"></div>
            <div class="w-4/6 pt-1">
              • <span class="font-bold">Simple mindset tweaks</span> about sales that will help you increase your conversion rates. 
            </div>
          </div>

          <div class="flex flex-row space-x-5 pl-12">
            <div class="p-9 bg-no-repeat" style="background-image:url(../images/checked.PNG) "></div>
            <div class="w-4/6 pt-1">
              • <span class="font-bold">Strategies to better qualify your leads</span> so that you’re only talking to the most qualified prospects for your programs or services.
            </div>
          </div>

          <div class="flex flex-row space-x-5 pl-12">
            <div class="p-9 bg-no-repeat" style="background-image:url(../images/checked.PNG) "></div>
            <div class="w-4/6 pt-1">
              • <span class="font-bold">How to create a solid sales system in your business,</span> so that you can build consistent revenue and sustainability in your business.
            </div>
          </div>

          <div class="flex flex-row space-x-5 pl-12">
            <div class="p-9 bg-no-repeat" style="background-image:url(../images/checked.PNG) "></div>
            <div class="w-4/6 pt-1">
              • <span class="font-bold">How to develop an authentic conversational approach to enrolling your ideal clients</span> so that you show up to your sales calls confidently
              and courageously as the leader (or expert) they are excited to work with. 
            </div>
          </div>

          <div class="flex flex-row space-x-5 pl-11">
            <div class="p-9 bg-no-repeat" style="background-image:url(../images/checkedpluz.PNG) "></div>
            <div class="w-4/6 pt-2">
              • <span class="italic">Plus, I will be doing Sales System makeover sessions, and introducing you to some of my clients in a powerful panel!</span>
            </div>
          </div>
        </div>
      </div>

      <div class="flex flex-row w-full bg-slate-700 mt-2 mb-4 space-x-10 lg:py-12 lg:my-0 lg:-translate-y-96">
        <div class="py-10 ml-10 sm:ml-12 md:ml-16 lg:ml-20 lg:py-2 xl:ml-28 2xl:ml-36">
          <button class="arno custom__color2 text-white font-bold rounded-md text-base px-4 pt-2 pb-1
            sm:text-xl sm:px-6 sm:pt-3 sm:pb-1.5
            md:text-2xl md:px-6 md:my-8 md:pt-4 md:pb-2
            lg:text-3xl lg:px-8 lg:my-12 lg:pt-6 lg:pb-4
            xl:text-4xl xl:px-10 xl:my-16
            2xl:text-5xl 2xl:px-10 2xl:my-20
            ">
              REGISTER&nbsp;NOW
          </button>
        </div>
        
        <div class="italic text-white text-xs py-4 pr-5
          sm:text-sm sm:py-4
          md:text-base md:py-12 md:translate-y-4
          lg:text-lg lg:translate-y-2
          xl:text-xl xl:translate-y-5
          2xl:text-xl 2xl:translate-y-9
          ">
            In addition to the opportunity to apply and for the <span class="font-bold">BONUS VIP Private Masterclass on March 24th</span>, you’ll also be invited to apply
            to receive a sales system makeover with me live at the one day virtual event (details and application provided upon registration).
        </div>
      </div>

      <div class="custom__color3 boder-b-2 border-gray-400 w-full pt-8 pb-20 lg:-translate-y-96">
        <div class="flex justify-center">
          <div class="arno text-white text-3xl border-b border-white w-10/12
          sm:text-3xl sm:pt-4
          md:text-4xl md:pt-6
          lg:text-5xl lg:pt-8
          xl:text-6xl lg:pt-10
          2xl:text-7xl 2xl:pt-12
          ">
            SURGE DETAILS
          </div>
        </div>
        <div class="custom__color3 my-5">

          <div class="flex justify-center">
            <div class="w-10/12 bg-white text-center">
              <div class="tracking-wide text-base pb-2 pt-12
                sm:text-lg sm:pt-14 sm:pb-4
                md:text-xl md:pt-16 md:pb-4
                lg:text-2xl lg:pt-120 lg:pb-4
                xl:text-3xl xl:pt-24 xl:pb-6
                2xl:pt-24 2xl:pb-8
                ">
                  AGENDA
              </div>
              <div class="garamond text-2xl
                md:text-3xl
                xl:text-4xl
                ">
                  Tuesday, March 23, 2021
              </div>
              <div class="garamond text-2xl
                md:text-3xl
                xl:text-4xl
                ">
                  10:00 a.m. - 3:00 p.m. Mountain Time
              </div>
              <div class="font-light text-base
                md:text-lg
                xl:text-xl
                ">
                  High-Content Virtual Workshop, Revenue Makeovers and Open Q&A
              </div>
              <div class="pt-6 tracking-widest text-base
                sm:text-lg
                md:text-xl
                lg:text-2xl lg:pt-8
                xl:text-2xl xl:pt-10
                2xl:text-3xl 2xl:pt-14
                ">
                  INVESTMENT
              </div>
              <div class="arno custom__color text-2xl pt-1
                sm:text-2xl sm:pb-2
                md:text-3xl md:pb-4
                lg:text-3xl lg:pb-4
                xl:text-4xl xl:pb-6
                2xl:text-4xl 2xl:pb-6 
                ">
                  Only &nbsp;$&nbsp;
                                    <span class="absolute -translate-y-0.5 -translate-x-2 
                                    sm:-translate-x-2
                                    md:-translate-x-2
                                    lg:-translate-x-2
                                    xl:-translate-x-3
                                    2xl:-translate-y-0.5 2xl:-translate-x-3">
                                    4</span>
                                    &nbsp;9! 
              </div>
              <div class="font-light italic text-base
                md:text-lg
                xl:text-xl">
                  *Formal details will be provided via email after you secure your seat. 
              </div>
              <div class="font-light italic text-base
                md:text-lg
                xl:text-xl
                ">
                  *All sales are final, no refunds will be issued*
              </div>

              <div class="flex justify-center">
                <div class="arno text-2xl w-11/12 pt-8
                  md:text-3xl
                  xl:text-4xl xl:w-10/12
                  ">
                    And, once you secure your seat, you can apply for an on-site business makeover
                    AND an opportunity to join my Private Advanced Masterclass on Wednesday, March 24th.
                </div>
              </div>

              <div class="flex justify-center pb-16 ">
                <button class="w-2/5 arno custom__color2 text-white font-bold rounded-md text-lg px-4 mt-6 pt-2 pb-0.5
                sm:text-xl sm:mt-8 sm:pt-3 sm:pb-1.5
                md:text-2xl md:mt-10 md:pt-4 md:pb-2
                lg:text-3xl lg:mt-10
                xl:text-4xl xl:mt-16 xl:pt-6 xl:pb-3.5
                2xl:text-5xl 2xl:my-12 2xl:pt-8 2xl:pb-5
                  ">
                    REGISTER&nbsp; NOW
                </button>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    
      <div class="garamond flex justify-center pt-6 sm:pt-8 md:pt-10 lg:py-0 lg:-translate-y-72">
        <p class="font-light text-base w-5/6
          sm:text-lg
          md:text-xl
          lg:text-xl
          xl:text-2xl
          ">
            When you join me for Surge, I will help you to end the maddening cycle of feast or famine in your business so that you create consistent cash flow.
            I’ll walk you through my strategic process so that you’ll have the strategy and clarity necessary to generate consistent revenue in your business.
            <br><br>
            And, if you act NOW, you can also apply to join me in the virtual “love seat” for a <span class="custom__color font-bold">Sales System Makeover Session</span>.<br>I will be doing a few of these 
            during the LIVE event. This is my free gift to those of you who take action NOW and register to join me. And, even if you’re not chosen, you will 
            learn from watching the other breakthroughs that occur in the room.
            <br><br>
            <span class="custom__color font-bold">All attendees will also have an opportunity to apply to join me for my BONUS VIP private Masterclass</span> on Wednesday, March 24th at no additional 
            investment (more details provided after you secure your seat).
        </p>
      </div>

      <div class="flex justify-center pb-16 mx-5
          md:mx-28 md:px-2 md:pt-2
          lg:mx-40 lg:py-0 lg:my-0 lg:-translate-y-56
          xl:-translate-y-64 xl:pt-4 
          2xl:pt-4 2xl:mx-44
          ">
        <button class="w-2/5 arno custom__color2 text-white font-bold rounded-md text-lg px-4 mt-6 pt-2 pb-0.5
        sm:text-xl sm:mt-8 sm:pt-3 sm:pb-1.5
        md:text-2xl md:mt-10 md:pt-4 md:pb-2
        lg:text-3xl lg:mt-10
        xl:text-4xl xl:mt-16 xl:pt-6 xl:pb-3.5
        2xl:text-5xl 2xl:my-12 2xl:pt-8 2xl:pb-5
          ">
            REGISTER&nbsp; NOW
        </button>
      </div>

      <div class="flex flex-col pb-5 lg:-translate-y-36 xl:-translate-y-48 gap-y-1">

        <div class="flex gap-1">
          <div class="w-9/12 py-24 sm:py-28 md:py-40 xl:py-56 2xl:py-64 bg-no-repeat bg-cover" style="background-image:url(../images/bundle1.PNG);background-size:100% 100%"></div>
          <div class="w-3/12 bg-no-repeat bg-cover" style="background-image:url(../images/bundle2.PNG);background-size:100% 100%"></div>
        </div>

        <div class="flex gap-1">
          <div class="w-3/6 py-24 sm:py-28 md:py-40 xl:py-56 2xl:py-64 bg-no-repeat bg-cover" style="background-image:url(../images/bundle3.PNG);background-size:100% 100%"></div>
          <div class="w-3/6 bg-no-repeat bg-cover" style="background-image:url(../images/bundle4.PNG);background-size:100% 100%"></div>
        </div>

        <div class="flex gap-1">
          <div class="w-5/12 py-16 sm:py-20 md:py-28 xl:py-40  2xl:py-56 bg-no-repeat bg-cover" style="background-image:url(../images/bundle5.PNG);background-size:100% 100%"></div>
          <div class="w-3/12 bg-no-repeat bg-contain" style="background-image:url(../images/bundle6.PNG);background-size:100% 100%"></div>
          <div class="w-5/12 bg-no-repeat bg-cover" style="background-image:url(../images/bundle7.PNG);background-size:100% 100%"></div>
        </div>
      </div>

      <div class="flex justify-end mt-6 sm:mt-12 lg:mt-0 xl:-translate-y-16">
        <img class="w-2/6 z-10 order-1 translate-x-20
          sm:translate-x-24
          md:translate-x-28
          lg:translate-x-48 lg:scale-90 lg:-translate-y-16
          xl:translate-x-3/4 xl:scale-75 xl:-translate-y-20" src="{{ url("images/client.PNG") }}" alt="img">

        <div class="w-4/6 z-0 order-2 custom__color3 translate-y-12
          lg:scale-y-90
          xl:scale-y-75
          2xl:scale-y-75
          ">
          <div class="ml-24 pt-4
            sm:ml-32 sm:pt-4
            md:ml-40 md:pt-8
            lg:ml-64 lg:pt-16
            xl:ml-80 xl:pl-14
            2xl:ml-96 2xl:pl-24 2xl:pt-24
            ">
            <h1 class="text-lg text-slate-700
              sm:text-xl sm:pt-10
              md:text-2xl
              lg:text-3xl
              xl:text-4xl xl:pt-20
              2xl:text-5xl
              ">
                La'Vista Jones
            </h1>
            <h2 class="text-white tracking-wide text-base 
              sm:text-lg
              md:text-xl
              lg:text-2xl
              xl:text-2xl
              2xl:text-3xl
              ">
               OPERATIONS MANAGER
            </h2>
            <h2 class="text-white tracking-wide text-base
              sm:text-lg
              md:text-xl
              lg:text-2xl
              xl:text-2xl
              2xl:text-3xl
              ">
                31 MARKETPLACE
            </h2>
            <p class="text-white text-sm py-2
              sm:text-base sm:pt-2 sm:pr-10
              md:text-lg md:pr-20
              lg:text-xl lg:pt-8 lg:pr-30
              xl:text-2xl xl:pt-12 xl:pr-36
              2xl:text-3xl 2xl:pr-24
              ">
              By simply following the steps Dr. Nadia provided for having sales conversations...
              <span class="font-bold">I was able to increase my monthly revenue by &nbsp;
                <span class="absolute translate-y-0.5 -translate-x-2
                  sm:-translate-x-2.5
                  lg:-translate-x-3
                  xl:-translate-x-3.5
                  2xl:translate-y-1 2xl:-translate-x-4">
                    4
                </span>80%!</span>
            </p>
          </div>
        </div>
      </div>

      <div class="flex justify-start mt-24 lg:mt-40 xl:-translate-y-2 xl:mt-6">
        <img class="w-2/6 z-10 order-2 -translate-x-20
          sm:-translate-x-24 sm:mt-6
          md:-translate-x-32 md:mt-6
          lg:-translate-x-48 lg:scale-90 lg:-translate-y-16
          xl:-translate-x-3/4 xl:scale-75 xl:-translate-y-20" src="{{ url("images/client.PNG") }}" alt="img">

        <div class="w-4/6 z-0 order-1 custom__color3 translate-y-12
          sm:mt-6 sm:translate-y-12
          md:mt-6 md:translate-y-12
          lg:scale-y-90 lg:translate-y-12
          xl:scale-y-75
          ">
          <div class="pl-6 pt-4
            sm:pl-8 sm:pt-4
            md:pl-14 md:pt-8
            lg:pl-16 lg:pt-16
            xl:pl-36
            2xl:pl-28 2xl:pt-24
            ">
            <h1 class="text-lg text-slate-700
              sm:text-xl sm:pt-10
              md:text-2xl
              lg:text-3xl
              xl:text-4xl xl:pt-20
              2xl:text-5xl
              ">
                La'Vista Jones
            </h1>
            <h2 class="text-white tracking-wide text-base
              sm:text-lg
              md:text-xl
              lg:text-2xl
              xl:text-2xl
              2xl:text-3xl 2xl:tracking-widest
              ">
                OPERATIONS MANAGER
            </h2>
            <h2 class="text-white tracking-wide text-base
              sm:text-lg
              md:text-xl
              lg:text-2xl
              xl:text-2xl
              2xl:text-3xl 2xl:tracking-widest
              ">
                31 MARKETPLACE
            </h2>
            <p class="text-white text-sm py-2 pr-24
              sm:text-base sm:pt-2 sm:pr-32 sm:mr-2
              md:text-lg md:pr-44
              lg:text-xl lg:pt-8 lg:pr-64
              xl:text-2xl xl:pt-12 xl:pr-80 xl:mr-14
              2xl:text-3xl 2xl:pr-96
              ">
                By simply following the steps Dr. Nadia provided for having sales conversations...
              <span class="font-bold">I was able to increase my monthly revenue by &nbsp;
                <span class="absolute translate-y-0.5 -translate-x-2
                  sm:-translate-x-2.5
                  lg:-translate-x-3
                  xl:-translate-x-3.5
                  2xl:translate-y-1 2xl:-translate-x-4">
                    4
                </span>80%!</span>
            </p>
          </div>
        </div>
      </div>

      <article class="custom__color5 w-full mt-24
        sm:mt-32
        md:mt-30
        lg:mt-40
        xl:mt-24
        ">
        <h1 class="garamond text-lg text-center pt-10 pb-4 px-16
          sm:text-xl sm:pt-14 sm:pb-2 sm:px-28
          md:text-2xl md:pb-0 
          lg:text-3xl lg:pt-20 lg:pb-2 lg:pt-24
          xl:text-4xl xl:pt-28
          ">
            We have limited seating so clear your calendar and join me… 
        </h1>
        <p class="text-sm text-center pb-10 px-4
          sm:text-base sm:px-5 sm:pt-4 sm:pb-14
          md:text-lg md:px-12 md:mx-1 md:pt-6 
          lg:text-xl lg:px-14 lg:pb-24 
          xl:text-xl xl:px-32 xl:mx-2 xl:pb-28 
          2xl:text-2xl
          ">
            you will be so glad that you did. We’re holding this event virtually but only for a small number of people, so if you know you need this right now,
            don’t hesitate! (<span class="custom__color font-bold">After you register, link and other details will be provided…</span>)
        </p>
      </article>

      <div class="flex justify-center">
        <div class="text-base tracking-wide pt-5
        sm:text-lg sm:pt-7
        md:text-xl md:pt-8
        lg:text-2xl lg:pt-12
        xl:text-2xl xl:pt-14
        2xl:text-3xl 2xl:tracking-widest 2xl:pt-16">
          BE COURAGEOUS,
        </div>
      </div>
      <div class="flex justify-center ml-1">
          <div class="bg-cover bg-no-repeat py-6 w-24
           sm:py-8 sm:w-28
           md:py-8 md:w-32
           lg:py-10 lg:w-36
           xl:py-12 xl:w-40
           2xl:py-14 2xl:w-44
           " style="background-image: url(../svg/signature.svg);background-size:100% 100%"></div>
      </div>

      <div class="flex justify-center pb-16 mx-5
        md:mx-28 md:px-2 md:pt-6
        lg:mx-40 lg:py-0 lg:my-0
        2xl:pt-4 2xl:mx-44
        ">
        <button class="w-2/5 arno custom__color2 text-white font-bold rounded-md text-lg px-4 mt-6 pt-2 pb-0.5
          sm:text-xl sm:mt-8 sm:pt-3 sm:pb-1.5
          md:text-2xl md:mt-10 md:pt-4 md:pb-2
          lg:text-3xl lg:mt-10
          xl:text-4xl xl:mt-16 xl:pt-6 xl:pb-3.5
          2xl:text-5xl 2xl:my-12 2xl:pt-8 2xl:pb-5
          ">
            REGISTER&nbsp; NOW
        </button>
      </div>

      <div class="bg-neutral-100 w-full pl-6
        sm:pl-8
        md:pl-16
        lg:mt-16 lg:pl-16
        xl:mt-20 xl:pl-36">

        <div class="tracking-widest text-base pt-10
          sm:pt-14 sm:text-lg
          md:text-2xl
          lg:text-3xl lg:pt-20 
          xl:text-3xl xl:pt-28
          ">
            STILL
        </div>

        <div class="arno custom__color text-2xl 
          sm:text-2xl
          md:text-3xl
          lg:text-4xl
          xl:text-4xl
          ">
            Undecided?
        </div>

        <div class="flex pt-2">
          <h1 class="w-2/6 text-sm
            sm:text-base
            md:text-lg
            lg:text-xl
            xl:text-xl
            2xl:text-2xl
            ">
              Fill out the form below and a member of our team will contact you to help you decide
              if this is the right next step for you to continue growing your business.
          </h1>
        
          <div class="flex flex-col space-y-2 w-4/6 px-6
            sm:px-8
            md:px-16 md:space-y-4
            lg:px-16 lg:space-y-4
            xl:px-24
            2xl:px-36
            ">
            <div class="flex flex-row space-x-2 text-xs
              sm:space-x-3 sm:text-sm
              md:space-x-4 md:text-base
              lg:space-x-4 lg:text-base
              ">
              <input class="custom__border rounded-sm placeholder-pink-500 w-3/6 p-1" type="text" placeholder="FIRST NAME">
              <input class="custom__border rounded-sm placeholder-pink-500 w-3/6 p-1" type="text" placeholder="LAST NAME">
            </div>

            <div class="flex flex-row space-x-2 text-xs
              sm:space-x-3 sm:text-sm
              md:space-x-4 md:text-base
              lg:space-x-4 lg:text-base
              ">
              <input class="custom__border rounded-sm placeholder-pink-500 w-3/6 p-1" type="text" placeholder="EMAIL">
              <input class="custom__border rounded-sm placeholder-pink-500 w-3/6 p-1" type="text" placeholder="PHONE">
            </div>

            <div>
              <textarea class="custom__border rounded-sm placeholder-pink-500 w-full text-xs p-1
                sm:space-x-3 sm:text-sm
                md:space-x-4 md:text-base
                lg:space-x-4 lg:text-base" 
                  placeholder="QUESTION" name="question" id="question" cols="30" rows="5"></textarea>
            </div>

            <div class="flex justify-end w-full">
              <button class="custom__color2 w-3/6 text-white font-bold rounded-sm text-sm px-2 mt-2 pt-1 pb-0.5 mb-8
                sm:text-base sm:mt-8 sm:pt-2 sm:pb-1.5 sm:mb-10
                md:text-lg md:mt-10 md:pt-3 md:pb-2
                lg:text-xl lg:mt-10
                xl:text-2xl xl:mt-16 xl:pt-4 xl:pb-4
                2xl:text-3xl 2xl:my-12 2xl:pt-5 2xl:pb-5
                ">
                  CONTACT US
              </button>
            </div>

          </div>
        </div>
      </div>

      <footer class="p-2 mt-0.5 bg-slate-700 flex flex-col justify-center md:flex-row md:justify-between">
        <span class="text-sm text-white sm:text-center dark:text-gray-400 m-auto lg:text-lg">© 2022. | <a href="https://thedoyenneagency.com/" class="hover:underline" target="_blank">the Doyenne Agency</a> | All Rights Reserved.
        </span>
        <ul class="flex justify-center md:justify-between items-center mt-1 sm:mt-0 m-auto">
            <li>
                <a href="#" class="mr-4 text-sm text-white hover:underline md:mr-6 dark:text-gray-400 lg:text-lg">Privacy Policy</a>
            </li>
            <li>
                <a href="#" class="mr-4 text-sm text-white hover:underline md:mr-6 dark:text-gray-400 lg:text-lg">Terms of Service</a>
            </li>
        </ul>
    </footer>

    </body>
</html>
